package it.unipd.dei.se.Enum;

public class DocumentFieldsEnum {

        public static final String ID = "id";
        public static final String BODY = "body";
        public static final String CONCLUSION = "conclusion";
        public static final String TEXT = "text";
        public static final String SOURCETEXT = "sourcetext";
        public static final String AUTHOR = "author";
        public static final String RUNID = "bladeGroup";
}
