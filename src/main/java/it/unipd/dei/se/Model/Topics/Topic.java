package it.unipd.dei.se.Model.Topics;

public class Topic {
    public int number;
    public String title;
    public String description;
    public String narrative;
}
