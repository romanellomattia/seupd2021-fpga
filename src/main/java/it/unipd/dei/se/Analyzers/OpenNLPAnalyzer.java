package it.unipd.dei.se.Analyzers;

import it.unipd.dei.se.Filters.OpenNLPNERFilter;
import opennlp.tools.namefind.TokenNameFinderModel;
import opennlp.tools.postag.POSModel;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.miscellaneous.TypeAsSynonymFilter;
import org.apache.lucene.analysis.opennlp.OpenNLPLemmatizerFilter;
import org.apache.lucene.analysis.opennlp.OpenNLPPOSFilter;
import org.apache.lucene.analysis.opennlp.OpenNLPTokenizer;
import org.apache.lucene.analysis.opennlp.tools.NLPNERTaggerOp;
import org.apache.lucene.analysis.opennlp.tools.NLPPOSTaggerOp;
import org.apache.lucene.analysis.opennlp.tools.OpenNLPOpsFactory;
import org.apache.lucene.analysis.util.ClasspathResourceLoader;
import org.apache.lucene.analysis.util.ResourceLoader;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import static it.unipd.dei.se.Analyzers.AnalyzerUtils.*;

public class OpenNLPAnalyzer extends Analyzer {

    /**
     * Creates a new instance of the analyzer.
     */
    public OpenNLPAnalyzer() {
        super();
    }

    @Override
    protected TokenStreamComponents createComponents(String fieldName) {

        ResourceLoader resourceLoader = new ClasspathResourceLoader(ClassLoader.getSystemClassLoader());

        Tokenizer source;
        try {
            source = new OpenNLPTokenizer(TokenStream.DEFAULT_TOKEN_ATTRIBUTE_FACTORY,
                    loadSentenceDetectorModel("en-sent.bin"), loadTokenizerModel("en-token.bin"));
        } catch (IOException e) {
            // The OpenNLPTokenizer seems to have a "wrong" signature declaring to throw an IOException which actually
            // is never thrown. This forces us to wrap everything with try-catch.

            throw new IllegalStateException(
                    String.format("Unable to create the OpenNLPTokenizer: %s. This should never happen: surprised :-o",
                            e.getMessage()), e);
        }

        /*TokenStream tokens = new OpenNLPPOSFilter(source, loadPosTaggerModel("en-pos-maxent.bin"));


        // Unfortunately the TypeAttribute is not stored in the index, so we need to work around this by adding the type
        // as a synonym token, which is also convenient at search time - see e.g.
        // https://fabian-kostadinov.github.io/2018/10/01/introduction-to-lucene-opennlp-part2/]
        tokens = new TypeAsSynonymFilter(tokens, "<nlp>");*/

        //NLPPOSTaggerOp posTaggerOp = loadPosTaggerModel("en-pos-maxent.bin");
        //TokenStream tokens = new OpenNLPPOSFilter(source, posTaggerOp);

        TokenStream tokens = new OpenNLPNERFilter(source, loadLNerTaggerModel("en-ner-location.bin"));

        tokens = new OpenNLPNERFilter(tokens, loadLNerTaggerModel("en-ner-person.bin"));

        tokens = new OpenNLPNERFilter(tokens, loadLNerTaggerModel("en-ner-organization.bin"));

        //tokens = new OpenNLPNERFilter(tokens, loadLNerTaggerModel("en-ner-money.bin"));

        //tokens = new OpenNLPNERFilter(tokens, loadLNerTaggerModel("en-ner-date.bin"));

        //tokens = new OpenNLPNERFilter(tokens, loadLNerTaggerModel("en-ner-time.bin"));

        tokens = new OpenNLPLemmatizerFilter(tokens, loadLemmatizerModel("en-lemmatizer.bin"));

        return new TokenStreamComponents(source, tokens);
    }
}
