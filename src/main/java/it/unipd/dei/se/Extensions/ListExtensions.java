package it.unipd.dei.se.Extensions;

import java.util.List;

public class ListExtensions {

    public static boolean isNullOrEmpty(List<?> list)
    {
        if(list == null)
            return true;
        if(list.isEmpty())
            return true;
        return false;
    }

}
