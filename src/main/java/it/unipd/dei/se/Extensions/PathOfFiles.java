package it.unipd.dei.se.Extensions;

import java.util.ArrayList;
import java.util.List;

public class PathOfFiles {
    public static final String IDEBATE = "experiment/idebate.json";
    public static final String PARLIAMENTARY = "experiment/parliamentary.json";
    public static final String DEBATEWISE = "experiment/debatewise.json";
    public static final String DEBATEPEDIA = "experiment/debatepedia.json";
    public static final String DEBATEORG = "experiment/debateorg.json";

    public static ArrayList<String> getInputFiles(boolean getAll)
    {
        ArrayList<String> list = new ArrayList<>();
        list.add(PARLIAMENTARY);
        if(getAll) {
            list.add(IDEBATE);
            list.add(DEBATEPEDIA);
            list.add(DEBATEWISE);
            list.add(DEBATEORG);
        }

        return list;
    }

    public static String getNormalFile(String file)
    {
        if(file == PARLIAMENTARY)
            return PARLIAMENTARY;
        return null;
    }
}


