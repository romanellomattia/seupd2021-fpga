package it.unipd.dei.se.Extensions;

import it.unipd.dei.se.Core.Indexer;
import it.unipd.dei.se.Enum.DocumentFieldsEnum;
import it.unipd.dei.se.Enum.StanceEnum;
import it.unipd.dei.se.Index.BodyField;
import it.unipd.dei.se.Model.Argument;
import it.unipd.dei.se.Model.RetrievedDocument;
import it.unipd.dei.se.Model.Stance;
import it.unipd.dei.se.ViewModel.ArgumentViewModel;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class ArgumentsExtensions {

    private ArrayList<RetrievedDocument> retrieved_PRO_documents;
    private ArrayList<RetrievedDocument> retrieved_CON_documents;
    private ArrayList<RetrievedDocument> retrieved_OTHER_documents;


    private ArrayList<RetrievedDocument> all_retrieved_documents;

    private String ID = DocumentFieldsEnum.ID;


    public ArgumentsExtensions()
    {
        resetData();
    }


    public void resetData()
    {
        retrieved_PRO_documents = new ArrayList<>();
        retrieved_CON_documents = new ArrayList<>();
        retrieved_OTHER_documents = new ArrayList<>();
        all_retrieved_documents = new ArrayList<>();
    }


    public static Document getDocumentFromArgument(Argument argument, String pathOfFile,String luceneID,String stance)
    {
        String body = getBodyLucene(argument);

        Document d = new Document();
        d.add(new StringField(DocumentFieldsEnum.ID, argument.id, Field.Store.YES));
        d.add(new BodyField(body));
        d.add(new StringField("file",pathOfFile, Field.Store.YES));
        d.add(new StringField("luceneID",luceneID, Field.Store.YES));
        d.add(new StringField("stance",stance, Field.Store.YES));

        return d;
    }

    private static String getBodyLucene(Argument argument)
    {
        String text = "";
        if(!ListExtensions.isNullOrEmpty(argument.premises))
            text = argument.premises.get(0).text;

        String [] sourceText = new String[]{
                argument.context.sourceText,
                text,
                argument.conclusion
        };

        return String.join(" ", sourceText);
    }


    public void addRetrieved(Document document, double score, int position)
    {
            var stance = document.get("stance");
            if (stance!=null) {
                int id = Integer.parseInt(document.get("luceneID"));

                var doc = new RetrievedDocument(document,score,id, position);
                all_retrieved_documents.add(doc);

                if (stance.equals(Stance.PRO))
                    retrieved_PRO_documents.add(doc);
                else if (stance.equals(Stance.CON))
                    retrieved_CON_documents.add(doc);
                else
                    retrieved_OTHER_documents.add(doc);
            }

    }

    public List<RetrievedDocument> getRetrievedDocuments(StanceEnum stance){
        if(stance == StanceEnum.PRO)
            return retrieved_PRO_documents;
        else if(stance == StanceEnum.CON)
            return retrieved_CON_documents;
        else
            return retrieved_OTHER_documents;
    }



    public void printDocument(RetrievedDocument document, Indexer indexer, PrintWriter out) throws IOException {
        if(document == null)
            throw new IllegalArgumentException("Document cannot be null");

        Argument argument = indexer.findAndPrintDocument(document);
        ArgumentViewModel viewModel = new ArgumentViewModel(
                argument,document.score,
                Integer.parseInt(document.document.get("luceneID")),
                document.position);
        viewModel.print(out);
    }

}
