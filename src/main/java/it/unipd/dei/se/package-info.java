/*
 * Copyright 2021 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * <i>Search Engines Project</i> .
 *
 * <p>Copyright and license information can be found in the file LICENSE.  Additional information can be found in the
 * file NOTICE.</p>
 *
 * @author    Mattia Romanello (mattia.romanello@studenti.unipd.it),
 *  		  Stefano Romanello (stefano.romanello.1@studenti.unipd.it)
 *  		  Leonardo Rossi (leonardo.rossi.3@studenti.unipd.it)
 *  		  Filippo Ghedin (filippo.ghedin@studenti.unipd.it)
 *  		  Matteo Benedetti (matteo.benedetti.4@studenti.unipd.it)
 * @version 1.00
 * @since 1.00
 */
package it.unipd.dei.se;