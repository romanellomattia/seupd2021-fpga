/*
 * Copyright 2021 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.se;

import it.unipd.dei.se.Analyzers.OpenNLPAnalyzer;
import it.unipd.dei.se.Core.Indexer;
import it.unipd.dei.se.Core.Searcher;
import it.unipd.dei.se.Enum.DocumentFieldsEnum;
import it.unipd.dei.se.Enum.StanceEnum;
import it.unipd.dei.se.Extensions.ArgumentsExtensions;
import it.unipd.dei.se.Model.Argument;
import it.unipd.dei.se.Model.RetrievedDocument;
import it.unipd.dei.se.Model.Topics.Topic;
import it.unipd.dei.se.Model.Topics.Topics;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.core.StopFilterFactory;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.en.EnglishMinimalStemFilterFactory;
import org.apache.lucene.analysis.en.EnglishPossessiveFilterFactory;
import org.apache.lucene.analysis.en.KStemFilterFactory;
import org.apache.lucene.analysis.en.PorterStemFilterFactory;
import org.apache.lucene.analysis.miscellaneous.WordDelimiterGraphFilterFactory;
import org.apache.lucene.analysis.ngram.NGramFilterFactory;
import org.apache.lucene.analysis.ngram.NGramTokenFilter;
import org.apache.lucene.analysis.ngram.NGramTokenizerFactory;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.apache.lucene.analysis.synonym.SynonymGraphFilterFactory;
import org.apache.lucene.analysis.tr.ApostropheFilterFactory;
import org.apache.lucene.queryparser.classic.ParseException;
import org.glassfish.jersey.internal.inject.Custom;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.List;
import java.util.Scanner;

public class BladeIR {

    private static final String DEFAULT_RUN_FILE = "experiment/out.txt";

    private static boolean print = false;
    private static boolean clean = false;
    private static boolean indexIsEmpty = false;
    private static boolean searchAll = false;
    private static boolean indexAll = false;
    private static Topic chosenTopic = null;

    public static void main(String[] args) throws Exception {

        final Searcher searcher;
        final FileSystem fs = FileSystems.getDefault();
        final Path outPath = fs.getPath(DEFAULT_RUN_FILE);
        final PrintWriter out =  new PrintWriter(Files.newBufferedWriter(outPath, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE));

        if (args.length == 3)
        {
            searcher = new Searcher(args[0], args[1],args[2]);
        }
        else
        {
            searcher = new Searcher();
        }

        //Load the topics for displaying them to the user
        final List<Topic> topics = searcher.parseTopics();

        //Retrive the settings from the user.
        //The informations are saved in the global variables of this class
        getUserResponse(searcher,topics);

        //ArgumentsExtensions is used to add and retrieve the documents
        final ArgumentsExtensions argumentsExtensions = new ArgumentsExtensions();
        final Indexer indexer = new Indexer(indexAll);

        final StandardAnalyzer standardAnalyzer = new StandardAnalyzer();

        final CustomAnalyzer.Builder analyzerFilterBuilder = CustomAnalyzer.builder()
                .withTokenizer(StandardTokenizerFactory.class)
                .addTokenFilter(LowerCaseFilterFactory.class)
                .addTokenFilter(ApostropheFilterFactory.class)
                .addTokenFilter(EnglishPossessiveFilterFactory.class)
                .addTokenFilter(WordDelimiterGraphFilterFactory.class)
                .addTokenFilter(StopFilterFactory.class, "words", "snowball.txt", "format", "snowball");

        final CustomAnalyzer docAnalyzer = analyzerFilterBuilder.build();

        final CustomAnalyzer queryAnalyzer = analyzerFilterBuilder
                .addTokenFilter(SynonymGraphFilterFactory.class, "synonyms", "synonyms_en.txt")
                .build();

        final OpenNLPAnalyzer openNLPAnalyzer = new OpenNLPAnalyzer();

        if(clean) {
            searcher.cleanIndexFolder();
            indexer.computeIndexing(docAnalyzer);
        }

        if(!indexIsEmpty)
        {
            if(searchAll)
            {
                //Search all the topics
                for (Topic tp : topics) {

                    //Perform the search action and add the results in the ArgumentsExtensions object
                    //Search and append in the run.txt file the result of this search.
                    searcher.search(Integer.toString(tp.number), tp, queryAnalyzer, argumentsExtensions);
                    if (print) {
                        printArgument(argumentsExtensions, tp, out, indexer);
                    }
                }
            }
            else
            {
                searcher.search(Integer.toString(chosenTopic.number), chosenTopic, queryAnalyzer, argumentsExtensions);
                if (print) {
                    printArgument(argumentsExtensions, chosenTopic, out, indexer);
                }
            }
        }
        else
        {
            System.out.println("Index folder is Empty");
        }

        if(print) {
            System.out.println("\nOpening file...");
            OpenFile();
        }

        out.close();
        searcher.close();
    }

    public static boolean convertIntToBool(int num)
    {
        if(num == 0 || num < 0)
            return false;
        return true;
    }

    public static void getUserResponse(Searcher searcher, List<Topic> Topics) throws Exception {

        Scanner sc=new Scanner(System.in);
        System.out.println("Do you want to clean the index folder? (1 = yes, 0 = no)");
        clean = convertIntToBool(sc.nextInt());
        if(clean)
        {
            System.out.println("Do you want to index all documents? (1 = yes, 0 = no (only parliamentary.json)");
            indexAll = convertIntToBool(sc.nextInt());
        }

        System.out.println("Do you want to print the retrieved argument by searching in files (can take a lot of time)? (1 = yes, 0 = no, 2 = use default)");
        int value = sc.nextInt();
        print = convertIntToBool(value);
        if (print)
            System.out.println("output will be available on out.txt");
        if(value != 2) { //use default value
            System.out.println("Do you want to search for each topic? (1 = yes, 0 = no)");
            searchAll = convertIntToBool(sc.nextInt());
        }
        if(!searchAll)
        {
            for (Topic topic: Topics) {
                System.out.println("id: "+Integer.toString(topic.number) + " title: " +topic.title);
            }
            System.out.println("\nChoose one topic by id from the above list:");
            int id = sc.nextInt();
            Topic topic = Topics.stream().filter(ci -> ci.number == id).findFirst().orElse(null);
            if(topic != null)
            {
                chosenTopic = topic;
            }
            else
            {
                throw new Exception("TOPIC WITH ID "+id+" DOES NOT EXIST");
            }
        }


        sc.close();

        indexIsEmpty = searcher.indexFolderIsEmpty();
    }

    public static void printArgument(ArgumentsExtensions argumentsExtensions,Topic tp, PrintWriter out, Indexer indexer) throws IOException {
        out.println("------------------------------------------------------");
        out.println("TOPIC: " + tp.title);
        out.println("------------------------------------------------------");

        System.out.println("printing pro arguments > out.txt ...");
        out.println("########################");
        out.println("TOP PRO");
        out.println("########################");
        for (RetrievedDocument doc : argumentsExtensions.getRetrievedDocuments(StanceEnum.PRO)) {
            argumentsExtensions.printDocument(doc, indexer, out);
            System.out.println("printing document " + doc.document.get(DocumentFieldsEnum.ID)+" ...");
        }

        System.out.println("\nprinting con arguments > out.txt ...");
        out.println("########################");
        out.println("TOP CON");
        out.println("########################");
        for (RetrievedDocument doc : argumentsExtensions.getRetrievedDocuments(StanceEnum.CON)) {
            argumentsExtensions.printDocument(doc, indexer, out);
            System.out.println("printing document " + doc.document.get(DocumentFieldsEnum.ID)+" ...");
        }
    }

    public static void OpenFile() throws IOException {
        File file = new File(DEFAULT_RUN_FILE);
        Desktop.getDesktop().open(file);
    }
}
